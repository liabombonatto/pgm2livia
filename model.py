from config import *

class Coelho (db.Entity):
    nome = Required(str)
    cor = Required(str)
    genero = Optional(str)
    raca = Required (str)
    dono = Required (str)
    def __str__(self):
        return f'{self.nome}, {self.cor}, {self.genero}, {self.raca}, {self.dono}'

db.bind(provider='sqlite', filename='person.db', create_db=True)
db.generate_mapping(create_tables=True)
