from config import *
from model import *

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/listar_coelho")
def listar_coelho ():
    with db_session:
        # obtém as coelho
        coelho = Coelho.select() 
        return render_template("listar_coelho.html", coelho = Coelho)

@app.route("/form_adicionar_coelho")
def form_adicionar_coelho():
    return render_template("form_adicionar_pessoa.html")

@app.route("/adicionar_coelho")
def adicionar_coelho():
    # obter os parâmetros
    nome = request.args.get("nome")
    cor = request.args.get("cor")
    genero = request.args.get("genero")
    raca = request.args.get("raca")
    dono = request.args.get("dono")

    # salvar
    with db_session:
        # criar a coelho
        c = Coelho(**request.args)
        # salvar
        commit()
        # encaminhar de volta para a listagem
        return redirect("listar_coelho") 

'''
run:
$ flask run
'''